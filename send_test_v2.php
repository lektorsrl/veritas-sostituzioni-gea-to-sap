<?php
ini_set('soap.wsdl_cache_enabled', 0);
ini_set('soap.wsdl_cache_ttl', 0);

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
/*
 * This file aims to show you how to use this generated package.
 * In addition, the goal is to show which methods are available and the fist needed parameter(s)
 * You have to use an associative array such as:
 * - the key must be a constant beginning with WSDL_ from AbstractSoapClientbase class each generated ServiceType class extends this class
 * - the value must be the corresponding key value (each option matches a {@link http://www.php.net/manual/en/soapclient.soapclient.php} option)
 * $options = array(
 * \WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_URL => 'http://localhost/gea4.wsdl',
 * \WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_TRACE => true,
 * \WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_LOGIN => 'you_secret_login',
 * \WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_PASSWORD => 'you_secret_password',
 * );
 * etc.....
 */
require_once __DIR__ . '/vendor/autoload.php';
/**
 * Minimal options
 */
$context = stream_context_create([
    'ssl' => [
        // set some SSL/TLS specific options
        'verify_peer' => false,
        'verify_peer_name' => false,
        'allow_self_signed' => true
    ]
]);

$options = array(
    \WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_URL => 'http://localhost/SostituzioniSendData/gea_sviluppo.wsdl',
    \WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_CLASSMAP => ClassMap::get(),
    \WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_LOGIN => 'SOA_GEA4',
    \WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_PASSWORD => '}/S+qtZZ(Cpjlx4Wi3=NAg7VNEYl%{eEWLMW%e~Y',
    \WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_STREAM_CONTEXT => $context
);


$zW = new \ServiceType\ZW($options);

// Database GEA
$serversql = "S02mysql";
$utentedb = "gea5";
$passworddb = "TxjRAozL";
$database = "veritas_it_gea_4_mestre";
$database_system = "veritas_it_gea_4";

$mysqli = new mysqli($serversql, $utentedb, $passworddb, $database);
if ($mysqli->connect_errno) {
    echo ("errore collegamento al db" . $mysqli->connect_error);
    exit();
}
$mysqli_system = new mysqli($serversql, $utentedb, $passworddb, $database_system);
if ($mysqli_system->connect_errno) {
    echo ("errore collegamento al db system" . $mysqli_system->connect_error);
    exit();
}

$sql = "select l.*, 
            motivo_sostituzione.act_value as motivo_sostituzione,
            altro_materiale.act_description as altro_materiale,
            ore_c.act_description as ore_c
            from letture l 
        left join letture_attivita motivo_sostituzione on l.id = motivo_sostituzione.id_lettura 
            and motivo_sostituzione.act_name = 'motivo_sostituzione' 
            and motivo_sostituzione.id_lettura = l.id
        left join letture_attivita altro_materiale on l.id = altro_materiale.id_lettura 
            and altro_materiale.act_name = 'altro_materiale' 
            and altro_materiale.id_lettura = l.id
        left join letture_attivita ore_c on l.id = ore_c.id_lettura 
            and ore_c.act_name = 'ore_c' 
            and ore_c.id_lettura = l.id
        where l.stato = 'LET' and l.periodicita = 'Y'
        group by l.id
        order by l.id";

$result = $mysqli->query($sql);

while ($row = mysqli_fetch_assoc($result)) {
    $id_record =  $row['id'];
    $ente_id = $row['codice_ente']; // 2= ente veritas
    $extra = explode("*", $row['extra']);
    $data_lettura = $row['data_lettura'];
    $ora_lettura = $row['ora_lettura'];
    $classe_nuova = catalogo($row['classe_nuova'], 'descrizione',$ente_id);
    $marca_nuova = catalogo($row['marca_nuova'], 'descrizione',$ente_id);
    $cdl = $extra[5];
    
    //CID operatori
    if($ente_id == 2 ){
        $operatori = operatori_mezzi(substr($data_lettura, -4) . "-" . substr($data_lettura, 3, 2) . "-" . substr($data_lettura, 0, 2) . " " . $ora_lettura, $row['terminale'], 'operatori');
        $operatori = explode("-", $operatori);
        $operatori = array_filter($operatori);
        $op = [];
        foreach ($operatori as $operatore) {
            $op[] =  new \StructType\ZwsSostContCid($operatore);
        }
    }else{
        $op = [];
    }
    $tcid = new \StructType\TableOfZwsSostContCid($op);
    unset($op);

    //TNotePosizione -> note nuove
    $notap = "";
    $notap_1 = $row['note_nuove'];
    $nota[] = new \StructType\Tline($notap, $notap_1);
    $notapos = new \StructType\TableOfTline($nota);
    if (strlen($notap_1) == 0) {
        $notapos = null;
    }
    unset($nota);

    //TNOteSost -> note letturista
    $notas = "";
    $notas_1 = substr($row['nota'], 0, 34);
    $nota123[] = new \StructType\Tline($notas, $notas_1);
    $notasost = new \StructType\TableOfTline($nota123);
    if (strlen($notas_1) == 0) {
        $notasost = null;
    }
    unset($nota123);

    $acc = $row['acc_nuova'];

    //XAttivitaMezzo
    if($ente_id == 2 ){
        $codiceMezzo = operatori_mezzi(substr($data_lettura, -4) . "-" . substr($data_lettura, 3, 2) . "-" . substr($data_lettura, 0, 2) . " " . $ora_lettura, $row['terminale'], 'mezzo');
        $XAttivitaMezzo = catalogo($codiceMezzo, 'codice',$ente_id);
    }else{
        $XAttivitaMezzo ="";
    }    
    $XAvviso = $extra[0];
    if($ente_id == 2 ){
        $XConsuntivare = substr($row['altro_materiale'], 0, 1);
    }else{
        $XConsuntivare = "";
    }   
    $XCoordinate = $row['latitudine'] . ";" . $row['longitudine'];
    $esito = ($row['lettura_eseguita'] == 'E') ? "OK" : "NO";

    if ($esito == "OK") {
        $XEsito = catalogo($row["motivo_sostituzione"], 'codice', $ente_id);
    } else {
        $XEsito = "N".$row['segn1'];
    }

    //FOTO1
    if ($row['flag_foto_1'] == 1) {
        list($foto1, $filesize1) = foto($row['progressivo'], $row['sequenza'], 1);
        $tipofoto = ($row['lettura_eseguita'] == 'N') ? "IM" : "LC";
        $eseguita = ($row['lettura_eseguita'] == 'N') ? "N" : "S";
        $cod_terminale = "070";
        $numfoto = 1;
        $nomefile =  formatStr($row['pdr'], 'N', 10) . $eseguita . substr($data_lettura, -4) . substr($data_lettura, 3, 2) . substr($data_lettura, 0, 2) . str_replace(":", "", $ora_lettura) . $cod_terminale . $numfoto . ".jpg";
        $XFoto_01 = new \StructType\ZwsSostContFoto(
            $tipofoto,
            $nomefile,
            'jpg',
            $filesize1,
            $foto1
        );
    }

    //FOTO2
    if ($row['flag_foto_2'] == 1) {
        list($foto2, $filesize2)  = foto($row['progressivo'], $row['sequenza'], 2);
        $eseguita = ($row['lettura_eseguita'] == 'N') ? "N" : "S";
        $cod_terminale = "070";
        $numfoto = 2;
        $nomefile =  formatStr($row['pdr'], 'N', 10) . $eseguita . substr($data_lettura, -4) . substr($data_lettura, 3, 2) . substr($data_lettura, 0, 2) . str_replace(":", "", $ora_lettura) . $cod_terminale . $numfoto . ".jpg";
        $XFoto_02 = new \StructType\ZwsSostContFoto(
            'AP',
            $nomefile,
            'jpg',
            $filesize2,
            $foto2
        );
    } else {
        $XFoto_02 = null;
    }
    //FOTO3
    if ($row['flag_foto_3'] == 1) {
        list($foto3, $filesize3) = foto($row['progressivo'], $row['sequenza'], 3);
        $eseguita = ($row['lettura_eseguita'] == 'N') ? "N" : "S";
        $cod_terminale = "070";
        $numfoto = 3;
        $nomefile =  formatStr($row['pdr'], 'N', 10) . $eseguita . substr($data_lettura, -4) . substr($data_lettura, 3, 2) . substr($data_lettura, 0, 2) . str_replace(":", "", $ora_lettura) . $cod_terminale . $numfoto . ".jpg";
        $XFoto_03 = new \StructType\ZwsSostContFoto(
            'LA',
            $nomefile,
            'jpg',
            $filesize3,
            $foto3
        );
    } else {
        $XFoto_03 = null;
    }
    //FOTO4
    if ($row['flag_foto_4'] == 1) {
        list($foto4, $filesize4) = foto($row['progressivo'], $row['sequenza'], 4);
        $eseguita = ($row['lettura_eseguita'] == 'N') ? "N" : "S";
        $cod_terminale = "070";
        $numfoto = 4;
        $nomefile =  formatStr($row['pdr'], 'N', 10) . $eseguita . substr($data_lettura, -4) . substr($data_lettura, 3, 2) . substr($data_lettura, 0, 2) . str_replace(":", "", $ora_lettura) . $cod_terminale . $numfoto . ".jpg";
        $XFoto_04 = new \StructType\ZwsSostContFoto(
            'AD',
            $nomefile,
            'jpg',
            $filesize4,
            $foto4
        );
    } else {
        $XFoto_04 = null;
    }
    //FOTO FIRMA
    list($foto5, $filesize5) = foto($row['progressivo'], $row['sequenza'], "F");
    if ($foto5 != "") {
        $eseguita = ($row['lettura_eseguita'] == 'N') ? "N" : "S";
        $cod_terminale = "070";
        $numfoto = 5;
        $nomefile =  formatStr($row['pdr'], 'N', 10) . $eseguita . substr($data_lettura, -4) . substr($data_lettura, 3, 2) . substr($data_lettura, 0, 2) . str_replace(":", "", $ora_lettura) . $cod_terminale . $numfoto . ".jpg";
        $XFoto_05 = new \StructType\ZwsSostContFoto(
            'FR',
            $nomefile,
            'jpg',
            $filesize5,
            $foto5
        );
    } else {
        $XFoto_05 = null;
    }

    $XFoto_06 = "";
    $XFoto_07 = "";
    $XFoto_08 = "";
    $XFoto_09 = "";
    $XFoto_10 = "";

    $xImpianto = $row['pdr'];

    //******* XIntervento *************
    $data_intervento = substr($data_lettura, -4) . "-" . substr($data_lettura, 3, 2) . "-" . substr($data_lettura, 0, 2);
    if($ente_id == 2 ){
        $ora_inizio = new datetime($ora_lettura);
    } else {
        $ora_inizio = '00:00:00';
    }    

    if($ente_id == 2 ){
        $minuti_consuntivati = $row['ore_c']; 
        $ora_inizio = date_modify($ora_inizio, "-$minuti_consuntivati minutes");
        $ora_inizio = date_format($ora_inizio, "H:i:s");
       
        switch ($row['ore_c']) {
            case "10":
                $ore_consuntivo = 0.16;
                break;
            case "20":
                $ore_consuntivo = 0.33;
                break;
            case "30":
                $ore_consuntivo = 0.5;
                break;
            case "60":
                $ore_consuntivo = 1;
                break;
            case "90":
                $ore_consuntivo = 1.5;
                break;
            case "120":
                $ore_consuntivo = 2;
                break;
            case "150":
                $ore_consuntivo = 2.5;
                break;
            default:
                $ore_consuntivo = 0.0;
        }
    }else{
        $ore_consuntivo = 0.0;
    }
    $intervento = new \StructType\ZwsSostContIntervento($data_intervento, $ora_inizio, $data_intervento, $ora_lettura, $ore_consuntivo);

    //******* XNuovoCont *************
    $nuovoctr = new \StructType\ZwsSostContCont(
        $row['matricola_nuova'],
        $marca_nuova,
        "A" . substr($classe_nuova, -3) . formatStr($row['cifre_nuove'], 'N', 2),
        intval($row['lettura_nuova']),
        $data_intervento,
        $ora_lettura = str_replace(":", "", substr($ora_lettura, 0, 5))
    );

    $XOrdine = $extra[3];
    $XPosizione = $row['ubicazione_nuova'];

    //******* XPreceCont *************
    $vecchioctr = new \StructType\ZwsSostContCont(
        $row['matricola'],
        $extra[10],
        "A" . substr($row['note'], 2, 3) . formatStr($row['cifre'], 'N', 2),
        intval($row['lettura']),
        $data_intervento,
        str_replace(":", "", substr($ora_lettura, 0, 5))
    );
    if($ente_id == 2 ){
        $xTargaMezzo = substr(catalogo($codiceMezzo, 'descrizione',$ente_id), 0, 7);
    }else{
        $xTargaMezzo = "";
    }  

    if($ente_id == 2 ){
        $XDittaEsterna = "";
    }else{
        $sql = "select ente_nome_dettaglio from ente where ente_id = ".$ente_id ;
        $result = $mysqli_system->query($sql);
        if ($result) {
            $row = $result->fetch_row();
            $XDittaEsterna = $row[0];
        } else {
            log_register("tutti", "tutti", "Error con ditta esterna", 2);
            exit("Error ditta esterna");
        }
    }    

    if ($zW->ZWsSostContatoriDatain(new \StructType\ZWsSostContatoriDatain(
        $tcid,
        $notapos,
        $notasost,
        null,
        $acc,
        $XAttivitaMezzo,
        $XAvviso,
        $XConsuntivare,
        $XCoordinate,
        $XDittaEsterna,
        $XEsito,
        $XFoto_01,
        $XFoto_02,
        $XFoto_03,
        $XFoto_04,
        $XFoto_05,
        null,
        null,
        null,
        null,
        null,
        $xImpianto,
        $intervento,
        $nuovoctr,
        $XOrdine,
        $XPosizione,
        $vecchioctr,
        $xTargaMezzo
    )) !== false) {
        print_r($zW->getResult());
        log_register($extra[0], $cdl, $zW->getResult(), 0);
        $success = TRUE;
    } else {
        print_r($zW->getLastError());
        log_register($extra[0], $cdl, $zW->getLastError(), 1);
        $success = FALSE;
    }

    if ($success == TRUE) {
        $query_update_gea = "update letture set stato = 'SPE' where id =" .$id_record . " and codice_ente = ".$ente_id." and stato = 'LET'";
        if ($mysqli->query($query_update_gea) === TRUE) {
            echo "Record passato allo stato SPE";
        } else {
            log_register($extra[0], $cdl, "errore nel passaggio allo SPE", 2);
            echo "Error updating record: " . $mysqli->error;
        }
    }
    //$mysqli->close();
}

function catalogo($id, $campo, $ente_id)
{
    global $mysqli;
    $sql = "select $campo from cataloghi where ente_id= $ente_id and id = $id";
    $result = $mysqli->query($sql);
    if ($result) {
        $row = $result->fetch_row();
        return $row[0];
    } else {
        log_register("tutti", "tutti", "Error cataloghi", 2);
        exit("Error cataloghi");
    }
}

function operatori_mezzi($dataOra, $terminale, $act_name)
{
    global $mysqli;
    $data = substr($dataOra, 0, 10) . " 00:00:00";
    $sql = "select act_value from attivita where data_ora >= '$data' and data_ora <= '$dataOra' and terminale='$terminale' and act_name = '$act_name' 
            order by data_ora desc limit 1";
    $result = $mysqli->query($sql);
    if ($result) {
        $row = $result->fetch_row();
        return $row[0];
    } else {
        log_register("tutti", "tutti", "Error operatorio_mezzi", 2);
        exit("Error operatori_mezzi");
    }
}

function foto($progressivo, $sequenza, $id)
{
    $path = "/docker/lektor/storage/foto/veritas_spa_mestre/";
    $path .= "lav_" . formatStr($progressivo, "N", 6) . "/";
    $progressivo = formatStr($progressivo, "N", 6);
    $sequenza = formatStr($sequenza, "N", 6);
    $fileName    = $progressivo . "_" . $sequenza . "_" . $id;
    for ($ic = 1; $ic <= 4; $ic++) {
        $cartella = substr($sequenza, $ic - 1, 1) . substr("000000", 0, 6 - $ic);
        $path .= $cartella . "/";
    }
    $pattern1 =  $path . $fileName . "*";
    $files = glob($pattern1);
    foreach ($files as $file) {
        $mtime = filemtime($file);
        $mtime_prec = $mtime;
        if ($mtime >= $mtime_prec) {
            $lastfile = $file;
        }
    }
    if (!empty($lastfile)) {
        $foto = base64_encode(file_get_contents($lastfile));
        $filesize = filesize($lastfile);
    } else {
        $foto = "";
        $filesize = 0;
    }
    return array($foto, $filesize);
}

function formatStr($s, $tipo, $lung)
{
    switch ($tipo) {
        case "N":
            $s = "000000000000000000000000000000000000000000000000000000000000000000000000000000000000" . $s;
            $s = substr($s, -1 * $lung);
            return $s;
            break;
    }
}

function log_register($avviso, $cdl, $message_full, $esito)
{
    global $mysqli;
    if ($esito == 1) {
        $message_short = $mysqli->real_escape_string($message_full["ServiceType\ZW::ZWsSostContatoriDatain"]->getMessage());
    } else {
        $message_short = "";
    }
    if ($esito == 2) {
        $esito = 1;
    }
    $message_full_escaped = $mysqli->real_escape_string(print_r($message_full, true));
    $sql = "insert into log_sostituzioni_gea_sap (avviso,cdl,message_short,message_full, esito) values 
            ('$avviso','$cdl','$message_short', '$message_full_escaped', $esito ) on duplicate key 
            update message_full = '$message_full_escaped', message_short = '$message_short', data=NOW()";
    if (mysqli_query($mysqli, $sql)) {
        echo "Creato record do log";
    } else {
        echo "Errore nella registrazione del log: " . $sql . "<br>" . mysqli_error($mysqli);
    }
}

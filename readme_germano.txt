corretto baco realtiva alla valdazione in ZwsSostContCont -> setlettura :


ora:
        if(mb_strpos($lettura, '.') > 0){
            if (!is_null($lettura) && mb_strlen(mb_substr($lettura, mb_strpos($lettura, '.') + 1)) > 0) {
                throw new \InvalidArgumentException(sprintf('Invalid2 value %s, the value must at most contain 0 fraction digits, %d given', var_export($lettura, true), mb_strlen(mb_substr($lettura, mb_strpos($lettura, '.') + 1))), __LINE__);
            }
        }

prima:
            if (!is_null($lettura) && mb_strlen(mb_substr($lettura, mb_strpos($lettura, '.') + 1)) > 0) {
                throw new \InvalidArgumentException(sprintf('Invalid2 value %s, the value must at most contain 0 fraction digits, %d given', var_export($lettura, true), mb_strlen(mb_substr($lettura, mb_strpos($lettura, '.') + 1))), __LINE__);
            }



Nel WSDL fornito da SAP bisogna correggere wsp:UsingPolicy wsdl:required="true" -> in false
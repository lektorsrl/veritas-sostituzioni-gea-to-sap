<?php

namespace ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for ZW ServiceType
 * @subpackage Services
 */
class ZW extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named ZWsSostContatoriDatain
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \StructType\ZWsSostContatoriDatain $parameters
     * @return \StructType\ZWsSostContatoriDatainResponse|bool
     */
    public function ZWsSostContatoriDatain(\StructType\ZWsSostContatoriDatain $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->ZWsSostContatoriDatain($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \StructType\ZWsSostContatoriDatainResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}

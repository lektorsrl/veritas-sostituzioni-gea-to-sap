<?php
/**
 * Class which returns the class map definition
 * @package
 */
class ClassMap
{
    /**
     * Returns the mapping between the WSDL Structs and generated Structs' classes
     * This array is sent to the \SoapClient when calling the WS
     * @return string[]
     */
    final public static function get()
    {
        return array(
            'ZwsSostContFoto' => '\\StructType\\ZwsSostContFoto',
            'ZwsSostContCont' => '\\StructType\\ZwsSostContCont',
            'Tline' => '\\StructType\\Tline',
            'Bapireturn1' => '\\StructType\\Bapireturn1',
            'ZwsSostContCid' => '\\StructType\\ZwsSostContCid',
            'ZwsSostContIntervento' => '\\StructType\\ZwsSostContIntervento',
            'TableOfTline' => '\\StructType\\TableOfTline',
            'TableOfBapireturn1' => '\\StructType\\TableOfBapireturn1',
            'TableOfZwsSostContCid' => '\\StructType\\TableOfZwsSostContCid',
            'ZWsSostContatoriDatain' => '\\StructType\\ZWsSostContatoriDatain',
            'ZWsSostContatoriDatainResponse' => '\\StructType\\ZWsSostContatoriDatainResponse',
        );
    }
}

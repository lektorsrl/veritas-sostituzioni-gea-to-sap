<?php

namespace StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Tline StructType
 * @subpackage Structs
 */
class Tline extends AbstractStructBase
{
    /**
     * The Tdformat
     * Meta information extracted from the WSDL
     * - base: xsd:string
     * - maxLength: 2
     * @var string
     */
    public $Tdformat;
    /**
     * The Tdline
     * Meta information extracted from the WSDL
     * - base: xsd:string
     * - maxLength: 132
     * @var string
     */
    public $Tdline;
    /**
     * Constructor method for Tline
     * @uses Tline::setTdformat()
     * @uses Tline::setTdline()
     * @param string $tdformat
     * @param string $tdline
     */
    public function __construct($tdformat = null, $tdline = null)
    {
        $this
            ->setTdformat($tdformat)
            ->setTdline($tdline);
    }
    /**
     * Get Tdformat value
     * @return string|null
     */
    public function getTdformat()
    {
        return $this->Tdformat;
    }
    /**
     * Set Tdformat value
     * @param string $tdformat
     * @return \StructType\Tline
     */
    public function setTdformat($tdformat = null)
    {
        // validation for constraint: string
        if (!is_null($tdformat) && !is_string($tdformat)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($tdformat, true), gettype($tdformat)), __LINE__);
        }
        // validation for constraint: maxLength(2)
        if (!is_null($tdformat) && mb_strlen($tdformat) > 2) {
            throw new \InvalidArgumentException(sprintf('Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 2', mb_strlen($tdformat)), __LINE__);
        }
        $this->Tdformat = $tdformat;
        return $this;
    }
    /**
     * Get Tdline value
     * @return string|null
     */
    public function getTdline()
    {
        return $this->Tdline;
    }
    /**
     * Set Tdline value
     * @param string $tdline
     * @return \StructType\Tline
     */
    public function setTdline($tdline = null)
    {
        // validation for constraint: string
        if (!is_null($tdline) && !is_string($tdline)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($tdline, true), gettype($tdline)), __LINE__);
        }
        // validation for constraint: maxLength(132)
        if (!is_null($tdline) && mb_strlen($tdline) > 132) {
            throw new \InvalidArgumentException(sprintf('Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 132', mb_strlen($tdline)), __LINE__);
        }
        $this->Tdline = $tdline;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \StructType\Tline
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}

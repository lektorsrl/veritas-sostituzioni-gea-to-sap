<?php

namespace StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ZwsSostContCont StructType
 * @subpackage Structs
 */
class ZwsSostContCont extends AbstractStructBase
{
    /**
     * The Matricola
     * Meta information extracted from the WSDL
     * - base: xsd:string
     * - maxLength: 30
     * @var string
     */
    public $Matricola;
    /**
     * The Produttore
     * Meta information extracted from the WSDL
     * - base: xsd:string
     * - maxLength: 30
     * @var string
     */
    public $Produttore;
    /**
     * The Materiale
     * Meta information extracted from the WSDL
     * - base: xsd:string
     * - maxLength: 18
     * @var string
     */
    public $Materiale;
    /**
     * The Lettura
     * Meta information extracted from the WSDL
     * - base: xsd:decimal
     * - fractionDigits: 0
     * - totalDigits: 17
     * @var float
     */
    public $Lettura;
    /**
     * The DataLett
     * Meta information extracted from the WSDL
     * - base: xsd:string
     * - maxLength: 10
     * - pattern: \d\d\d\d-\d\d-\d\d
     * @var string
     */
    public $DataLett;
    /**
     * The OraLett
     * Meta information extracted from the WSDL
     * - base: xsd:string
     * - maxLength: 4
     * @var string
     */
    public $OraLett;
    /**
     * Constructor method for ZwsSostContCont
     * @uses ZwsSostContCont::setMatricola()
     * @uses ZwsSostContCont::setProduttore()
     * @uses ZwsSostContCont::setMateriale()
     * @uses ZwsSostContCont::setLettura()
     * @uses ZwsSostContCont::setDataLett()
     * @uses ZwsSostContCont::setOraLett()
     * @param string $matricola
     * @param string $produttore
     * @param string $materiale
     * @param float $lettura
     * @param string $dataLett
     * @param string $oraLett
     */
    public function __construct($matricola = null, $produttore = null, $materiale = null, $lettura = null, $dataLett = null, $oraLett = null)
    {
        $this
            ->setMatricola($matricola)
            ->setProduttore($produttore)
            ->setMateriale($materiale)
            ->setLettura($lettura)
            ->setDataLett($dataLett)
            ->setOraLett($oraLett);
    }
    /**
     * Get Matricola value
     * @return string|null
     */
    public function getMatricola()
    {
        return $this->Matricola;
    }
    /**
     * Set Matricola value
     * @param string $matricola
     * @return \StructType\ZwsSostContCont
     */
    public function setMatricola($matricola = null)
    {
        // validation for constraint: string
        if (!is_null($matricola) && !is_string($matricola)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($matricola, true), gettype($matricola)), __LINE__);
        }
        // validation for constraint: maxLength(30)
        if (!is_null($matricola) && mb_strlen($matricola) > 30) {
            throw new \InvalidArgumentException(sprintf('Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 30', mb_strlen($matricola)), __LINE__);
        }
        $this->Matricola = $matricola;
        return $this;
    }
    /**
     * Get Produttore value
     * @return string|null
     */
    public function getProduttore()
    {
        return $this->Produttore;
    }
    /**
     * Set Produttore value
     * @param string $produttore
     * @return \StructType\ZwsSostContCont
     */
    public function setProduttore($produttore = null)
    {
        // validation for constraint: string
        if (!is_null($produttore) && !is_string($produttore)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($produttore, true), gettype($produttore)), __LINE__);
        }
        // validation for constraint: maxLength(30)
        if (!is_null($produttore) && mb_strlen($produttore) > 30) {
            throw new \InvalidArgumentException(sprintf('Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 30', mb_strlen($produttore)), __LINE__);
        }
        $this->Produttore = $produttore;
        return $this;
    }
    /**
     * Get Materiale value
     * @return string|null
     */
    public function getMateriale()
    {
        return $this->Materiale;
    }
    /**
     * Set Materiale value
     * @param string $materiale
     * @return \StructType\ZwsSostContCont
     */
    public function setMateriale($materiale = null)
    {
        // validation for constraint: string
        if (!is_null($materiale) && !is_string($materiale)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($materiale, true), gettype($materiale)), __LINE__);
        }
        // validation for constraint: maxLength(18)
        if (!is_null($materiale) && mb_strlen($materiale) > 18) {
            throw new \InvalidArgumentException(sprintf('Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 18', mb_strlen($materiale)), __LINE__);
        }
        $this->Materiale = $materiale;
        return $this;
    }
    /**
     * Get Lettura value
     * @return float|null
     */
    public function getLettura()
    {
        return $this->Lettura;
    }
    /**
     * Set Lettura value
     * @param float $lettura
     * @return \StructType\ZwsSostContCont
     */
    public function setLettura($lettura = null)
    {
        // validation for constraint: float
        if (!is_null($lettura) && !(is_float($lettura) || is_numeric($lettura))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($lettura, true), gettype($lettura)), __LINE__);
        }
        // validation for constraint: fractionDigits
        if(mb_strpos($lettura, '.') > 0){
            if (!is_null($lettura) && mb_strlen(mb_substr($lettura, mb_strpos($lettura, '.') + 1)) > 0) {
                throw new \InvalidArgumentException(sprintf('Invalid value %s, the value must at most contain 0 fraction digits, %d given', var_export($lettura, true), mb_strlen(mb_substr($lettura, mb_strpos($lettura, '.') + 1))), __LINE__);
            }
        }    
        // validation for constraint: totalDigits(17)
        if (!is_null($lettura) && mb_strlen(preg_replace('/(\D)/', '', $lettura)) > 17) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, the value must use at most 17 digits, "%d" given', var_export($lettura, true), mb_strlen(preg_replace('/(\D)/', '', $lettura))), __LINE__);
        }
        $this->Lettura = $lettura;
        return $this;
    }
    /**
     * Get DataLett value
     * @return string|null
     */
    public function getDataLett()
    {
        return $this->DataLett;
    }
    /**
     * Set DataLett value
     * @param string $dataLett
     * @return \StructType\ZwsSostContCont
     */
    public function setDataLett($dataLett = null)
    {
        // validation for constraint: string
        if (!is_null($dataLett) && !is_string($dataLett)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($dataLett, true), gettype($dataLett)), __LINE__);
        }
        // validation for constraint: maxLength(10)
        if (!is_null($dataLett) && mb_strlen($dataLett) > 10) {
            throw new \InvalidArgumentException(sprintf('Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 10', mb_strlen($dataLett)), __LINE__);
        }
        // validation for constraint: pattern(\d\d\d\d-\d\d-\d\d)
        if (!is_null($dataLett) && !preg_match('/\\d\\d\\d\\d-\\d\\d-\\d\\d/', $dataLett)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression \d\d\d\d-\d\d-\d\d', var_export($dataLett, true)), __LINE__);
        }
        $this->DataLett = $dataLett;
        return $this;
    }
    /**
     * Get OraLett value
     * @return string|null
     */
    public function getOraLett()
    {
        return $this->OraLett;
    }
    /**
     * Set OraLett value
     * @param string $oraLett
     * @return \StructType\ZwsSostContCont
     */
    public function setOraLett($oraLett = null)
    {
        // validation for constraint: string
        if (!is_null($oraLett) && !is_string($oraLett)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($oraLett, true), gettype($oraLett)), __LINE__);
        }
        // validation for constraint: maxLength(4)
        if (!is_null($oraLett) && mb_strlen($oraLett) > 4) {
            throw new \InvalidArgumentException(sprintf('Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 4', mb_strlen($oraLett)), __LINE__);
        }
        $this->OraLett = $oraLett;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \StructType\ZwsSostContCont
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}

<?php

namespace StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ZwsSostContIntervento StructType
 * @subpackage Structs
 */
class ZwsSostContIntervento extends AbstractStructBase
{
    /**
     * The DtInizio
     * Meta information extracted from the WSDL
     * - base: xsd:string
     * - maxLength: 10
     * - pattern: \d\d\d\d-\d\d-\d\d
     * @var string
     */
    public $DtInizio;
    /**
     * The HrInizio
     * Meta information extracted from the WSDL
     * - base: xsd:time
     * - pattern: [0-9]{2}:[0-9]{2}:[0-9]{2}
     * @var string
     */
    public $HrInizio;
    /**
     * The DtFine
     * Meta information extracted from the WSDL
     * - base: xsd:string
     * - maxLength: 10
     * - pattern: \d\d\d\d-\d\d-\d\d
     * @var string
     */
    public $DtFine;
    /**
     * The HrFine
     * Meta information extracted from the WSDL
     * - base: xsd:time
     * - pattern: [0-9]{2}:[0-9]{2}:[0-9]{2}
     * @var string
     */
    public $HrFine;
    /**
     * The OreConsunt
     * Meta information extracted from the WSDL
     * - base: xsd:decimal
     * - fractionDigits: 2
     * - totalDigits: 7
     * @var float
     */
    public $OreConsunt;
    /**
     * Constructor method for ZwsSostContIntervento
     * @uses ZwsSostContIntervento::setDtInizio()
     * @uses ZwsSostContIntervento::setHrInizio()
     * @uses ZwsSostContIntervento::setDtFine()
     * @uses ZwsSostContIntervento::setHrFine()
     * @uses ZwsSostContIntervento::setOreConsunt()
     * @param string $dtInizio
     * @param string $hrInizio
     * @param string $dtFine
     * @param string $hrFine
     * @param float $oreConsunt
     */
    public function __construct($dtInizio = null, $hrInizio = null, $dtFine = null, $hrFine = null, $oreConsunt = null)
    {
        $this
            ->setDtInizio($dtInizio)
            ->setHrInizio($hrInizio)
            ->setDtFine($dtFine)
            ->setHrFine($hrFine)
            ->setOreConsunt($oreConsunt);
    }
    /**
     * Get DtInizio value
     * @return string|null
     */
    public function getDtInizio()
    {
        return $this->DtInizio;
    }
    /**
     * Set DtInizio value
     * @param string $dtInizio
     * @return \StructType\ZwsSostContIntervento
     */
    public function setDtInizio($dtInizio = null)
    {
        // validation for constraint: string
        if (!is_null($dtInizio) && !is_string($dtInizio)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($dtInizio, true), gettype($dtInizio)), __LINE__);
        }
        // validation for constraint: maxLength(10)
        if (!is_null($dtInizio) && mb_strlen($dtInizio) > 10) {
            throw new \InvalidArgumentException(sprintf('Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 10', mb_strlen($dtInizio)), __LINE__);
        }
        // validation for constraint: pattern(\d\d\d\d-\d\d-\d\d)
        if (!is_null($dtInizio) && !preg_match('/\\d\\d\\d\\d-\\d\\d-\\d\\d/', $dtInizio)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression \d\d\d\d-\d\d-\d\d', var_export($dtInizio, true)), __LINE__);
        }
        $this->DtInizio = $dtInizio;
        return $this;
    }
    /**
     * Get HrInizio value
     * @return string|null
     */
    public function getHrInizio()
    {
        return $this->HrInizio;
    }
    /**
     * Set HrInizio value
     * @param string $hrInizio
     * @return \StructType\ZwsSostContIntervento
     */
    public function setHrInizio($hrInizio = null)
    {
        // validation for constraint: string
        if (!is_null($hrInizio) && !is_string($hrInizio)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($hrInizio, true), gettype($hrInizio)), __LINE__);
        }
        // validation for constraint: pattern([0-9]{2}:[0-9]{2}:[0-9]{2})
        if (!is_null($hrInizio) && !preg_match('/[0-9]{2}:[0-9]{2}:[0-9]{2}/', $hrInizio)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression [0-9]{2}:[0-9]{2}:[0-9]{2}', var_export($hrInizio, true)), __LINE__);
        }
        $this->HrInizio = $hrInizio;
        return $this;
    }
    /**
     * Get DtFine value
     * @return string|null
     */
    public function getDtFine()
    {
        return $this->DtFine;
    }
    /**
     * Set DtFine value
     * @param string $dtFine
     * @return \StructType\ZwsSostContIntervento
     */
    public function setDtFine($dtFine = null)
    {
        // validation for constraint: string
        if (!is_null($dtFine) && !is_string($dtFine)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($dtFine, true), gettype($dtFine)), __LINE__);
        }
        // validation for constraint: maxLength(10)
        if (!is_null($dtFine) && mb_strlen($dtFine) > 10) {
            throw new \InvalidArgumentException(sprintf('Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 10', mb_strlen($dtFine)), __LINE__);
        }
        // validation for constraint: pattern(\d\d\d\d-\d\d-\d\d)
        if (!is_null($dtFine) && !preg_match('/\\d\\d\\d\\d-\\d\\d-\\d\\d/', $dtFine)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression \d\d\d\d-\d\d-\d\d', var_export($dtFine, true)), __LINE__);
        }
        $this->DtFine = $dtFine;
        return $this;
    }
    /**
     * Get HrFine value
     * @return string|null
     */
    public function getHrFine()
    {
        return $this->HrFine;
    }
    /**
     * Set HrFine value
     * @param string $hrFine
     * @return \StructType\ZwsSostContIntervento
     */
    public function setHrFine($hrFine = null)
    {
        // validation for constraint: string
        if (!is_null($hrFine) && !is_string($hrFine)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($hrFine, true), gettype($hrFine)), __LINE__);
        }
        // validation for constraint: pattern([0-9]{2}:[0-9]{2}:[0-9]{2})
        if (!is_null($hrFine) && !preg_match('/[0-9]{2}:[0-9]{2}:[0-9]{2}/', $hrFine)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression [0-9]{2}:[0-9]{2}:[0-9]{2}', var_export($hrFine, true)), __LINE__);
        }
        $this->HrFine = $hrFine;
        return $this;
    }
    /**
     * Get OreConsunt value
     * @return float|null
     */
    public function getOreConsunt()
    {
        return $this->OreConsunt;
    }
    /**
     * Set OreConsunt value
     * @param float $oreConsunt
     * @return \StructType\ZwsSostContIntervento
     */
    public function setOreConsunt($oreConsunt = null)
    {
        // validation for constraint: float
        if (!is_null($oreConsunt) && !(is_float($oreConsunt) || is_numeric($oreConsunt))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($oreConsunt, true), gettype($oreConsunt)), __LINE__);
        }
        // validation for constraint: fractionDigits(2)
        if (!is_null($oreConsunt) && mb_strlen(mb_substr($oreConsunt, mb_strpos($oreConsunt, '.') + 1)) > 2) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, the value must at most contain 2 fraction digits, %d given', var_export($oreConsunt, true), mb_strlen(mb_substr($oreConsunt, mb_strpos($oreConsunt, '.') + 1))), __LINE__);
        }
        // validation for constraint: totalDigits(7)
        if (!is_null($oreConsunt) && mb_strlen(preg_replace('/(\D)/', '', $oreConsunt)) > 7) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, the value must use at most 7 digits, "%d" given', var_export($oreConsunt, true), mb_strlen(preg_replace('/(\D)/', '', $oreConsunt))), __LINE__);
        }
        $this->OreConsunt = $oreConsunt;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \StructType\ZwsSostContIntervento
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}

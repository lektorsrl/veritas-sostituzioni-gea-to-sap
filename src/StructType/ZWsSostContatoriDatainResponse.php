<?php

namespace StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ZWsSostContatoriDatainResponse StructType
 * @subpackage Structs
 */
class ZWsSostContatoriDatainResponse extends AbstractStructBase
{
    /**
     * The TCid
     * @var \StructType\TableOfZwsSostContCid
     */
    public $TCid;
    /**
     * The TNotePosizione
     * @var \StructType\TableOfTline
     */
    public $TNotePosizione;
    /**
     * The TNoteSost
     * @var \StructType\TableOfTline
     */
    public $TNoteSost;
    /**
     * The TReturn
     * @var \StructType\TableOfBapireturn1
     */
    public $TReturn;
    /**
     * The YAvviso
     * Meta information extracted from the WSDL
     * - base: xsd:string
     * - maxLength: 12
     * @var string
     */
    public $YAvviso;
    /**
     * The YClav
     * Meta information extracted from the WSDL
     * - base: xsd:string
     * - maxLength: 8
     * @var string
     */
    public $YClav;
    /**
     * The YImpianto
     * Meta information extracted from the WSDL
     * - base: xsd:string
     * - maxLength: 10
     * @var string
     */
    public $YImpianto;
    /**
     * Constructor method for ZWsSostContatoriDatainResponse
     * @uses ZWsSostContatoriDatainResponse::setTCid()
     * @uses ZWsSostContatoriDatainResponse::setTNotePosizione()
     * @uses ZWsSostContatoriDatainResponse::setTNoteSost()
     * @uses ZWsSostContatoriDatainResponse::setTReturn()
     * @uses ZWsSostContatoriDatainResponse::setYAvviso()
     * @uses ZWsSostContatoriDatainResponse::setYClav()
     * @uses ZWsSostContatoriDatainResponse::setYImpianto()
     * @param \StructType\TableOfZwsSostContCid $tCid
     * @param \StructType\TableOfTline $tNotePosizione
     * @param \StructType\TableOfTline $tNoteSost
     * @param \StructType\TableOfBapireturn1 $tReturn
     * @param string $yAvviso
     * @param string $yClav
     * @param string $yImpianto
     */
    public function __construct(\StructType\TableOfZwsSostContCid $tCid = null, \StructType\TableOfTline $tNotePosizione = null, \StructType\TableOfTline $tNoteSost = null, \StructType\TableOfBapireturn1 $tReturn = null, $yAvviso = null, $yClav = null, $yImpianto = null)
    {
        $this
            ->setTCid($tCid)
            ->setTNotePosizione($tNotePosizione)
            ->setTNoteSost($tNoteSost)
            ->setTReturn($tReturn)
            ->setYAvviso($yAvviso)
            ->setYClav($yClav)
            ->setYImpianto($yImpianto);
    }
    /**
     * Get TCid value
     * @return \StructType\TableOfZwsSostContCid|null
     */
    public function getTCid()
    {
        return $this->TCid;
    }
    /**
     * Set TCid value
     * @param \StructType\TableOfZwsSostContCid $tCid
     * @return \StructType\ZWsSostContatoriDatainResponse
     */
    public function setTCid(\StructType\TableOfZwsSostContCid $tCid = null)
    {
        $this->TCid = $tCid;
        return $this;
    }
    /**
     * Get TNotePosizione value
     * @return \StructType\TableOfTline|null
     */
    public function getTNotePosizione()
    {
        return $this->TNotePosizione;
    }
    /**
     * Set TNotePosizione value
     * @param \StructType\TableOfTline $tNotePosizione
     * @return \StructType\ZWsSostContatoriDatainResponse
     */
    public function setTNotePosizione(\StructType\TableOfTline $tNotePosizione = null)
    {
        $this->TNotePosizione = $tNotePosizione;
        return $this;
    }
    /**
     * Get TNoteSost value
     * @return \StructType\TableOfTline|null
     */
    public function getTNoteSost()
    {
        return $this->TNoteSost;
    }
    /**
     * Set TNoteSost value
     * @param \StructType\TableOfTline $tNoteSost
     * @return \StructType\ZWsSostContatoriDatainResponse
     */
    public function setTNoteSost(\StructType\TableOfTline $tNoteSost = null)
    {
        $this->TNoteSost = $tNoteSost;
        return $this;
    }
    /**
     * Get TReturn value
     * @return \StructType\TableOfBapireturn1|null
     */
    public function getTReturn()
    {
        return $this->TReturn;
    }
    /**
     * Set TReturn value
     * @param \StructType\TableOfBapireturn1 $tReturn
     * @return \StructType\ZWsSostContatoriDatainResponse
     */
    public function setTReturn(\StructType\TableOfBapireturn1 $tReturn = null)
    {
        $this->TReturn = $tReturn;
        return $this;
    }
    /**
     * Get YAvviso value
     * @return string|null
     */
    public function getYAvviso()
    {
        return $this->YAvviso;
    }
    /**
     * Set YAvviso value
     * @param string $yAvviso
     * @return \StructType\ZWsSostContatoriDatainResponse
     */
    public function setYAvviso($yAvviso = null)
    {
        // validation for constraint: string
        if (!is_null($yAvviso) && !is_string($yAvviso)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($yAvviso, true), gettype($yAvviso)), __LINE__);
        }
        // validation for constraint: maxLength(12)
        if (!is_null($yAvviso) && mb_strlen($yAvviso) > 12) {
            throw new \InvalidArgumentException(sprintf('Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 12', mb_strlen($yAvviso)), __LINE__);
        }
        $this->YAvviso = $yAvviso;
        return $this;
    }
    /**
     * Get YClav value
     * @return string|null
     */
    public function getYClav()
    {
        return $this->YClav;
    }
    /**
     * Set YClav value
     * @param string $yClav
     * @return \StructType\ZWsSostContatoriDatainResponse
     */
    public function setYClav($yClav = null)
    {
        // validation for constraint: string
        if (!is_null($yClav) && !is_string($yClav)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($yClav, true), gettype($yClav)), __LINE__);
        }
        // validation for constraint: maxLength(8)
        if (!is_null($yClav) && mb_strlen($yClav) > 8) {
            throw new \InvalidArgumentException(sprintf('Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 8', mb_strlen($yClav)), __LINE__);
        }
        $this->YClav = $yClav;
        return $this;
    }
    /**
     * Get YImpianto value
     * @return string|null
     */
    public function getYImpianto()
    {
        return $this->YImpianto;
    }
    /**
     * Set YImpianto value
     * @param string $yImpianto
     * @return \StructType\ZWsSostContatoriDatainResponse
     */
    public function setYImpianto($yImpianto = null)
    {
        // validation for constraint: string
        if (!is_null($yImpianto) && !is_string($yImpianto)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($yImpianto, true), gettype($yImpianto)), __LINE__);
        }
        // validation for constraint: maxLength(10)
        if (!is_null($yImpianto) && mb_strlen($yImpianto) > 10) {
            throw new \InvalidArgumentException(sprintf('Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 10', mb_strlen($yImpianto)), __LINE__);
        }
        $this->YImpianto = $yImpianto;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \StructType\ZWsSostContatoriDatainResponse
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}

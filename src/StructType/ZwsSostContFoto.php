<?php

namespace StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ZwsSostContFoto StructType
 * @subpackage Structs
 */
class ZwsSostContFoto extends AbstractStructBase
{
    /**
     * The TipoFoto
     * Meta information extracted from the WSDL
     * - base: xsd:string
     * - maxLength: 2
     * @var string
     */
    public $TipoFoto;
    /**
     * The NomeFile
     * Meta information extracted from the WSDL
     * - base: xsd:string
     * - maxLength: 60
     * @var string
     */
    public $NomeFile;
    /**
     * The Formato
     * Meta information extracted from the WSDL
     * - base: xsd:string
     * - maxLength: 3
     * @var string
     */
    public $Formato;
    /**
     * The Dimensione
     * @var int
     */
    public $Dimensione;
    /**
     * The ContFile
     * Meta information extracted from the WSDL
     * - base: xsd:string
     * @var string
     */
    public $ContFile;
    /**
     * Constructor method for ZwsSostContFoto
     * @uses ZwsSostContFoto::setTipoFoto()
     * @uses ZwsSostContFoto::setNomeFile()
     * @uses ZwsSostContFoto::setFormato()
     * @uses ZwsSostContFoto::setDimensione()
     * @uses ZwsSostContFoto::setContFile()
     * @param string $tipoFoto
     * @param string $nomeFile
     * @param string $formato
     * @param int $dimensione
     * @param string $contFile
     */
    public function __construct($tipoFoto = null, $nomeFile = null, $formato = null, $dimensione = null, $contFile = null)
    {
        $this
            ->setTipoFoto($tipoFoto)
            ->setNomeFile($nomeFile)
            ->setFormato($formato)
            ->setDimensione($dimensione)
            ->setContFile($contFile);
    }
    /**
     * Get TipoFoto value
     * @return string|null
     */
    public function getTipoFoto()
    {
        return $this->TipoFoto;
    }
    /**
     * Set TipoFoto value
     * @param string $tipoFoto
     * @return \StructType\ZwsSostContFoto
     */
    public function setTipoFoto($tipoFoto = null)
    {
        // validation for constraint: string
        if (!is_null($tipoFoto) && !is_string($tipoFoto)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($tipoFoto, true), gettype($tipoFoto)), __LINE__);
        }
        // validation for constraint: maxLength(2)
        if (!is_null($tipoFoto) && mb_strlen($tipoFoto) > 2) {
            throw new \InvalidArgumentException(sprintf('Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 2', mb_strlen($tipoFoto)), __LINE__);
        }
        $this->TipoFoto = $tipoFoto;
        return $this;
    }
    /**
     * Get NomeFile value
     * @return string|null
     */
    public function getNomeFile()
    {
        return $this->NomeFile;
    }
    /**
     * Set NomeFile value
     * @param string $nomeFile
     * @return \StructType\ZwsSostContFoto
     */
    public function setNomeFile($nomeFile = null)
    {
        // validation for constraint: string
        if (!is_null($nomeFile) && !is_string($nomeFile)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($nomeFile, true), gettype($nomeFile)), __LINE__);
        }
        // validation for constraint: maxLength(60)
        if (!is_null($nomeFile) && mb_strlen($nomeFile) > 60) {
            throw new \InvalidArgumentException(sprintf('Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 60', mb_strlen($nomeFile)), __LINE__);
        }
        $this->NomeFile = $nomeFile;
        return $this;
    }
    /**
     * Get Formato value
     * @return string|null
     */
    public function getFormato()
    {
        return $this->Formato;
    }
    /**
     * Set Formato value
     * @param string $formato
     * @return \StructType\ZwsSostContFoto
     */
    public function setFormato($formato = null)
    {
        // validation for constraint: string
        if (!is_null($formato) && !is_string($formato)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($formato, true), gettype($formato)), __LINE__);
        }
        // validation for constraint: maxLength(3)
        if (!is_null($formato) && mb_strlen($formato) > 3) {
            throw new \InvalidArgumentException(sprintf('Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 3', mb_strlen($formato)), __LINE__);
        }
        $this->Formato = $formato;
        return $this;
    }
    /**
     * Get Dimensione value
     * @return int|null
     */
    public function getDimensione()
    {
        return $this->Dimensione;
    }
    /**
     * Set Dimensione value
     * @param int $dimensione
     * @return \StructType\ZwsSostContFoto
     */
    public function setDimensione($dimensione = null)
    {
        // validation for constraint: int
        if (!is_null($dimensione) && !(is_int($dimensione) || ctype_digit($dimensione))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($dimensione, true), gettype($dimensione)), __LINE__);
        }
        $this->Dimensione = $dimensione;
        return $this;
    }
    /**
     * Get ContFile value
     * @return string|null
     */
    public function getContFile()
    {
        return $this->ContFile;
    }
    /**
     * Set ContFile value
     * @param string $contFile
     * @return \StructType\ZwsSostContFoto
     */
    public function setContFile($contFile = null)
    {
        // validation for constraint: string
        if (!is_null($contFile) && !is_string($contFile)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($contFile, true), gettype($contFile)), __LINE__);
        }
        $this->ContFile = $contFile;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \StructType\ZwsSostContFoto
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}

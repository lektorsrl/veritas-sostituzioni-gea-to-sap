<?php

namespace StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Bapireturn1 StructType
 * @subpackage Structs
 */
class Bapireturn1 extends AbstractStructBase
{
    /**
     * The Type
     * Meta information extracted from the WSDL
     * - base: xsd:string
     * - maxLength: 1
     * @var string
     */
    public $Type;
    /**
     * The Id
     * Meta information extracted from the WSDL
     * - base: xsd:string
     * - maxLength: 20
     * @var string
     */
    public $Id;
    /**
     * The Number
     * Meta information extracted from the WSDL
     * - base: xsd:string
     * - maxLength: 3
     * - pattern: \d*
     * @var string
     */
    public $Number;
    /**
     * The Message
     * Meta information extracted from the WSDL
     * - base: xsd:string
     * - maxLength: 220
     * @var string
     */
    public $Message;
    /**
     * The LogNo
     * Meta information extracted from the WSDL
     * - base: xsd:string
     * - maxLength: 20
     * @var string
     */
    public $LogNo;
    /**
     * The LogMsgNo
     * Meta information extracted from the WSDL
     * - base: xsd:string
     * - maxLength: 6
     * - pattern: \d*
     * @var string
     */
    public $LogMsgNo;
    /**
     * The MessageV1
     * Meta information extracted from the WSDL
     * - base: xsd:string
     * - maxLength: 50
     * @var string
     */
    public $MessageV1;
    /**
     * The MessageV2
     * Meta information extracted from the WSDL
     * - base: xsd:string
     * - maxLength: 50
     * @var string
     */
    public $MessageV2;
    /**
     * The MessageV3
     * Meta information extracted from the WSDL
     * - base: xsd:string
     * - maxLength: 50
     * @var string
     */
    public $MessageV3;
    /**
     * The MessageV4
     * Meta information extracted from the WSDL
     * - base: xsd:string
     * - maxLength: 50
     * @var string
     */
    public $MessageV4;
    /**
     * Constructor method for Bapireturn1
     * @uses Bapireturn1::setType()
     * @uses Bapireturn1::setId()
     * @uses Bapireturn1::setNumber()
     * @uses Bapireturn1::setMessage()
     * @uses Bapireturn1::setLogNo()
     * @uses Bapireturn1::setLogMsgNo()
     * @uses Bapireturn1::setMessageV1()
     * @uses Bapireturn1::setMessageV2()
     * @uses Bapireturn1::setMessageV3()
     * @uses Bapireturn1::setMessageV4()
     * @param string $type
     * @param string $id
     * @param string $number
     * @param string $message
     * @param string $logNo
     * @param string $logMsgNo
     * @param string $messageV1
     * @param string $messageV2
     * @param string $messageV3
     * @param string $messageV4
     */
    public function __construct($type = null, $id = null, $number = null, $message = null, $logNo = null, $logMsgNo = null, $messageV1 = null, $messageV2 = null, $messageV3 = null, $messageV4 = null)
    {
        $this
            ->setType($type)
            ->setId($id)
            ->setNumber($number)
            ->setMessage($message)
            ->setLogNo($logNo)
            ->setLogMsgNo($logMsgNo)
            ->setMessageV1($messageV1)
            ->setMessageV2($messageV2)
            ->setMessageV3($messageV3)
            ->setMessageV4($messageV4);
    }
    /**
     * Get Type value
     * @return string|null
     */
    public function getType()
    {
        return $this->Type;
    }
    /**
     * Set Type value
     * @param string $type
     * @return \StructType\Bapireturn1
     */
    public function setType($type = null)
    {
        // validation for constraint: string
        if (!is_null($type) && !is_string($type)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($type, true), gettype($type)), __LINE__);
        }
        // validation for constraint: maxLength(1)
        if (!is_null($type) && mb_strlen($type) > 1) {
            throw new \InvalidArgumentException(sprintf('Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 1', mb_strlen($type)), __LINE__);
        }
        $this->Type = $type;
        return $this;
    }
    /**
     * Get Id value
     * @return string|null
     */
    public function getId()
    {
        return $this->Id;
    }
    /**
     * Set Id value
     * @param string $id
     * @return \StructType\Bapireturn1
     */
    public function setId($id = null)
    {
        // validation for constraint: string
        if (!is_null($id) && !is_string($id)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($id, true), gettype($id)), __LINE__);
        }
        // validation for constraint: maxLength(20)
        if (!is_null($id) && mb_strlen($id) > 20) {
            throw new \InvalidArgumentException(sprintf('Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 20', mb_strlen($id)), __LINE__);
        }
        $this->Id = $id;
        return $this;
    }
    /**
     * Get Number value
     * @return string|null
     */
    public function getNumber()
    {
        return $this->Number;
    }
    /**
     * Set Number value
     * @param string $number
     * @return \StructType\Bapireturn1
     */
    public function setNumber($number = null)
    {
        // validation for constraint: string
        if (!is_null($number) && !is_string($number)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($number, true), gettype($number)), __LINE__);
        }
        // validation for constraint: maxLength(3)
        if (!is_null($number) && mb_strlen($number) > 3) {
            throw new \InvalidArgumentException(sprintf('Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 3', mb_strlen($number)), __LINE__);
        }
        // validation for constraint: pattern(\d*)
        if (!is_null($number) && !preg_match('/\\d*/', $number)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression \d*', var_export($number, true)), __LINE__);
        }
        $this->Number = $number;
        return $this;
    }
    /**
     * Get Message value
     * @return string|null
     */
    public function getMessage()
    {
        return $this->Message;
    }
    /**
     * Set Message value
     * @param string $message
     * @return \StructType\Bapireturn1
     */
    public function setMessage($message = null)
    {
        // validation for constraint: string
        if (!is_null($message) && !is_string($message)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($message, true), gettype($message)), __LINE__);
        }
        // validation for constraint: maxLength(220)
        if (!is_null($message) && mb_strlen($message) > 220) {
            throw new \InvalidArgumentException(sprintf('Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 220', mb_strlen($message)), __LINE__);
        }
        $this->Message = $message;
        return $this;
    }
    /**
     * Get LogNo value
     * @return string|null
     */
    public function getLogNo()
    {
        return $this->LogNo;
    }
    /**
     * Set LogNo value
     * @param string $logNo
     * @return \StructType\Bapireturn1
     */
    public function setLogNo($logNo = null)
    {
        // validation for constraint: string
        if (!is_null($logNo) && !is_string($logNo)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($logNo, true), gettype($logNo)), __LINE__);
        }
        // validation for constraint: maxLength(20)
        if (!is_null($logNo) && mb_strlen($logNo) > 20) {
            throw new \InvalidArgumentException(sprintf('Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 20', mb_strlen($logNo)), __LINE__);
        }
        $this->LogNo = $logNo;
        return $this;
    }
    /**
     * Get LogMsgNo value
     * @return string|null
     */
    public function getLogMsgNo()
    {
        return $this->LogMsgNo;
    }
    /**
     * Set LogMsgNo value
     * @param string $logMsgNo
     * @return \StructType\Bapireturn1
     */
    public function setLogMsgNo($logMsgNo = null)
    {
        // validation for constraint: string
        if (!is_null($logMsgNo) && !is_string($logMsgNo)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($logMsgNo, true), gettype($logMsgNo)), __LINE__);
        }
        // validation for constraint: maxLength(6)
        if (!is_null($logMsgNo) && mb_strlen($logMsgNo) > 6) {
            throw new \InvalidArgumentException(sprintf('Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 6', mb_strlen($logMsgNo)), __LINE__);
        }
        // validation for constraint: pattern(\d*)
        if (!is_null($logMsgNo) && !preg_match('/\\d*/', $logMsgNo)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression \d*', var_export($logMsgNo, true)), __LINE__);
        }
        $this->LogMsgNo = $logMsgNo;
        return $this;
    }
    /**
     * Get MessageV1 value
     * @return string|null
     */
    public function getMessageV1()
    {
        return $this->MessageV1;
    }
    /**
     * Set MessageV1 value
     * @param string $messageV1
     * @return \StructType\Bapireturn1
     */
    public function setMessageV1($messageV1 = null)
    {
        // validation for constraint: string
        if (!is_null($messageV1) && !is_string($messageV1)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($messageV1, true), gettype($messageV1)), __LINE__);
        }
        // validation for constraint: maxLength(50)
        if (!is_null($messageV1) && mb_strlen($messageV1) > 50) {
            throw new \InvalidArgumentException(sprintf('Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 50', mb_strlen($messageV1)), __LINE__);
        }
        $this->MessageV1 = $messageV1;
        return $this;
    }
    /**
     * Get MessageV2 value
     * @return string|null
     */
    public function getMessageV2()
    {
        return $this->MessageV2;
    }
    /**
     * Set MessageV2 value
     * @param string $messageV2
     * @return \StructType\Bapireturn1
     */
    public function setMessageV2($messageV2 = null)
    {
        // validation for constraint: string
        if (!is_null($messageV2) && !is_string($messageV2)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($messageV2, true), gettype($messageV2)), __LINE__);
        }
        // validation for constraint: maxLength(50)
        if (!is_null($messageV2) && mb_strlen($messageV2) > 50) {
            throw new \InvalidArgumentException(sprintf('Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 50', mb_strlen($messageV2)), __LINE__);
        }
        $this->MessageV2 = $messageV2;
        return $this;
    }
    /**
     * Get MessageV3 value
     * @return string|null
     */
    public function getMessageV3()
    {
        return $this->MessageV3;
    }
    /**
     * Set MessageV3 value
     * @param string $messageV3
     * @return \StructType\Bapireturn1
     */
    public function setMessageV3($messageV3 = null)
    {
        // validation for constraint: string
        if (!is_null($messageV3) && !is_string($messageV3)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($messageV3, true), gettype($messageV3)), __LINE__);
        }
        // validation for constraint: maxLength(50)
        if (!is_null($messageV3) && mb_strlen($messageV3) > 50) {
            throw new \InvalidArgumentException(sprintf('Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 50', mb_strlen($messageV3)), __LINE__);
        }
        $this->MessageV3 = $messageV3;
        return $this;
    }
    /**
     * Get MessageV4 value
     * @return string|null
     */
    public function getMessageV4()
    {
        return $this->MessageV4;
    }
    /**
     * Set MessageV4 value
     * @param string $messageV4
     * @return \StructType\Bapireturn1
     */
    public function setMessageV4($messageV4 = null)
    {
        // validation for constraint: string
        if (!is_null($messageV4) && !is_string($messageV4)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($messageV4, true), gettype($messageV4)), __LINE__);
        }
        // validation for constraint: maxLength(50)
        if (!is_null($messageV4) && mb_strlen($messageV4) > 50) {
            throw new \InvalidArgumentException(sprintf('Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 50', mb_strlen($messageV4)), __LINE__);
        }
        $this->MessageV4 = $messageV4;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \StructType\Bapireturn1
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}

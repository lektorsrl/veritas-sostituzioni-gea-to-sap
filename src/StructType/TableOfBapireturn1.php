<?php

namespace StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for TableOfBapireturn1 StructType
 * @subpackage Structs
 */
class TableOfBapireturn1 extends AbstractStructBase
{
    /**
     * The item
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \StructType\Bapireturn1[]
     */
    public $item;
    /**
     * Constructor method for TableOfBapireturn1
     * @uses TableOfBapireturn1::setItem()
     * @param \StructType\Bapireturn1[] $item
     */
    public function __construct(array $item = array())
    {
        $this
            ->setItem($item);
    }
    /**
     * Get item value
     * @return \StructType\Bapireturn1[]|null
     */
    public function getItem()
    {
        return $this->item;
    }
    /**
     * This method is responsible for validating the values passed to the setItem method
     * This method is willingly generated in order to preserve the one-line inline validation within the setItem method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateItemForArrayConstraintsFromSetItem(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $tableOfBapireturn1ItemItem) {
            // validation for constraint: itemType
            if (!$tableOfBapireturn1ItemItem instanceof \StructType\Bapireturn1) {
                $invalidValues[] = is_object($tableOfBapireturn1ItemItem) ? get_class($tableOfBapireturn1ItemItem) : sprintf('%s(%s)', gettype($tableOfBapireturn1ItemItem), var_export($tableOfBapireturn1ItemItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The item property can only contain items of type \StructType\Bapireturn1, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set item value
     * @throws \InvalidArgumentException
     * @param \StructType\Bapireturn1[] $item
     * @return \StructType\TableOfBapireturn1
     */
    public function setItem(array $item = array())
    {
        // validation for constraint: array
        if ('' !== ($itemArrayErrorMessage = self::validateItemForArrayConstraintsFromSetItem($item))) {
            throw new \InvalidArgumentException($itemArrayErrorMessage, __LINE__);
        }
        $this->item = $item;
        return $this;
    }
    /**
     * Add item to item value
     * @throws \InvalidArgumentException
     * @param \StructType\Bapireturn1 $item
     * @return \StructType\TableOfBapireturn1
     */
    public function addToItem(\StructType\Bapireturn1 $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \StructType\Bapireturn1) {
            throw new \InvalidArgumentException(sprintf('The item property can only contain items of type \StructType\Bapireturn1, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->item[] = $item;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \StructType\TableOfBapireturn1
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}

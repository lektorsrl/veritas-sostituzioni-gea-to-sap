<?php

namespace StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ZwsSostContCid StructType
 * @subpackage Structs
 */
class ZwsSostContCid extends AbstractStructBase
{
    /**
     * The Cid
     * Meta information extracted from the WSDL
     * - base: xsd:string
     * - maxLength: 8
     * - pattern: \d*
     * @var string
     */
    public $Cid;
    /**
     * Constructor method for ZwsSostContCid
     * @uses ZwsSostContCid::setCid()
     * @param string $cid
     */
    public function __construct($cid = null)
    {
        $this
            ->setCid($cid);
    }
    /**
     * Get Cid value
     * @return string|null
     */
    public function getCid()
    {
        return $this->Cid;
    }
    /**
     * Set Cid value
     * @param string $cid
     * @return \StructType\ZwsSostContCid
     */
    public function setCid($cid = null)
    {
        // validation for constraint: string
        if (!is_null($cid) && !is_string($cid)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($cid, true), gettype($cid)), __LINE__);
        }
        // validation for constraint: maxLength(8)
        if (!is_null($cid) && mb_strlen($cid) > 8) {
            throw new \InvalidArgumentException(sprintf('Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 8', mb_strlen($cid)), __LINE__);
        }
        // validation for constraint: pattern(\d*)
        if (!is_null($cid) && !preg_match('/\\d*/', $cid)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression \d*', var_export($cid, true)), __LINE__);
        }
        $this->Cid = $cid;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \StructType\ZwsSostContCid
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}

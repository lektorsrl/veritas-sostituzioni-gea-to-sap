<?php

namespace StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ZWsSostContatoriDatain StructType
 * @subpackage Structs
 */
class ZWsSostContatoriDatain extends AbstractStructBase
{
    /**
     * The TCid
     * @var \StructType\TableOfZwsSostContCid
     */
    public $TCid;
    /**
     * The TNotePosizione
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \StructType\TableOfTline
     */
    public $TNotePosizione;
    /**
     * The TNoteSost
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \StructType\TableOfTline
     */
    public $TNoteSost;
    /**
     * The TReturn
     * @var \StructType\TableOfBapireturn1
     */
    public $TReturn;
    /**
     * The XAccessibilita
     * Meta information extracted from the WSDL
     * - base: xsd:string
     * - maxLength: 1
     * @var string
     */
    public $XAccessibilita;
    /**
     * The XAttivitaMezzo
     * Meta information extracted from the WSDL
     * - base: xsd:string
     * - maxLength: 6
     * @var string
     */
    public $XAttivitaMezzo;
    /**
     * The XAvviso
     * Meta information extracted from the WSDL
     * - base: xsd:string
     * - maxLength: 12
     * @var string
     */
    public $XAvviso;
    /**
     * The XConsuntivare
     * Meta information extracted from the WSDL
     * - base: xsd:string
     * - maxLength: 1
     * @var string
     */
    public $XConsuntivare;
    /**
     * The XCoordinate
     * Meta information extracted from the WSDL
     * - base: xsd:string
     * - maxLength: 50
     * @var string
     */
    public $XCoordinate;
    /**
     * The XDittaEsterna
     * Meta information extracted from the WSDL
     * - base: xsd:string
     * - maxLength: 10
     * - minOccurs: 0
     * @var string
     */
    public $XDittaEsterna;
    /**
     * The XEsito
     * Meta information extracted from the WSDL
     * - base: xsd:string
     * - maxLength: 2
     * @var string
     */
    public $XEsito;
    /**
     * The XFoto_01
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \StructType\ZwsSostContFoto
     */
    public $XFoto_01;
    /**
     * The XFoto_02
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \StructType\ZwsSostContFoto
     */
    public $XFoto_02;
    /**
     * The XFoto_03
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \StructType\ZwsSostContFoto
     */
    public $XFoto_03;
    /**
     * The XFoto_04
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \StructType\ZwsSostContFoto
     */
    public $XFoto_04;
    /**
     * The XFoto_05
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \StructType\ZwsSostContFoto
     */
    public $XFoto_05;
    /**
     * The XFoto_06
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \StructType\ZwsSostContFoto
     */
    public $XFoto_06;
    /**
     * The XFoto_07
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \StructType\ZwsSostContFoto
     */
    public $XFoto_07;
    /**
     * The XFoto_08
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \StructType\ZwsSostContFoto
     */
    public $XFoto_08;
    /**
     * The XFoto_09
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \StructType\ZwsSostContFoto
     */
    public $XFoto_09;
    /**
     * The XFoto_10
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \StructType\ZwsSostContFoto
     */
    public $XFoto_10;
    /**
     * The XImpianto
     * Meta information extracted from the WSDL
     * - base: xsd:string
     * - maxLength: 10
     * @var string
     */
    public $XImpianto;
    /**
     * The XIntervento
     * @var \StructType\ZwsSostContIntervento
     */
    public $XIntervento;
    /**
     * The XNuovoCont
     * @var \StructType\ZwsSostContCont
     */
    public $XNuovoCont;
    /**
     * The XOrdine
     * Meta information extracted from the WSDL
     * - base: xsd:string
     * - maxLength: 12
     * @var string
     */
    public $XOrdine;
    /**
     * The XPosizione
     * Meta information extracted from the WSDL
     * - base: xsd:string
     * - maxLength: 8
     * @var string
     */
    public $XPosizione;
    /**
     * The XPreceCont
     * @var \StructType\ZwsSostContCont
     */
    public $XPreceCont;
    /**
     * The XTargaMezzo
     * Meta information extracted from the WSDL
     * - base: xsd:string
     * - maxLength: 15
     * @var string
     */
    public $XTargaMezzo;
    /**
     * Constructor method for ZWsSostContatoriDatain
     * @uses ZWsSostContatoriDatain::setTCid()
     * @uses ZWsSostContatoriDatain::setTNotePosizione()
     * @uses ZWsSostContatoriDatain::setTNoteSost()
     * @uses ZWsSostContatoriDatain::setTReturn()
     * @uses ZWsSostContatoriDatain::setXAccessibilita()
     * @uses ZWsSostContatoriDatain::setXAttivitaMezzo()
     * @uses ZWsSostContatoriDatain::setXAvviso()
     * @uses ZWsSostContatoriDatain::setXConsuntivare()
     * @uses ZWsSostContatoriDatain::setXCoordinate()
     * @uses ZWsSostContatoriDatain::setXDittaEsterna()
     * @uses ZWsSostContatoriDatain::setXEsito()
     * @uses ZWsSostContatoriDatain::setXFoto_01()
     * @uses ZWsSostContatoriDatain::setXFoto_02()
     * @uses ZWsSostContatoriDatain::setXFoto_03()
     * @uses ZWsSostContatoriDatain::setXFoto_04()
     * @uses ZWsSostContatoriDatain::setXFoto_05()
     * @uses ZWsSostContatoriDatain::setXFoto_06()
     * @uses ZWsSostContatoriDatain::setXFoto_07()
     * @uses ZWsSostContatoriDatain::setXFoto_08()
     * @uses ZWsSostContatoriDatain::setXFoto_09()
     * @uses ZWsSostContatoriDatain::setXFoto_10()
     * @uses ZWsSostContatoriDatain::setXImpianto()
     * @uses ZWsSostContatoriDatain::setXIntervento()
     * @uses ZWsSostContatoriDatain::setXNuovoCont()
     * @uses ZWsSostContatoriDatain::setXOrdine()
     * @uses ZWsSostContatoriDatain::setXPosizione()
     * @uses ZWsSostContatoriDatain::setXPreceCont()
     * @uses ZWsSostContatoriDatain::setXTargaMezzo()
     * @param \StructType\TableOfZwsSostContCid $tCid
     * @param \StructType\TableOfTline $tNotePosizione
     * @param \StructType\TableOfTline $tNoteSost
     * @param \StructType\TableOfBapireturn1 $tReturn
     * @param string $xAccessibilita
     * @param string $xAttivitaMezzo
     * @param string $xAvviso
     * @param string $xConsuntivare
     * @param string $xCoordinate
     * @param string $xDittaEsterna
     * @param string $xEsito
     * @param \StructType\ZwsSostContFoto $xFoto_01
     * @param \StructType\ZwsSostContFoto $xFoto_02
     * @param \StructType\ZwsSostContFoto $xFoto_03
     * @param \StructType\ZwsSostContFoto $xFoto_04
     * @param \StructType\ZwsSostContFoto $xFoto_05
     * @param \StructType\ZwsSostContFoto $xFoto_06
     * @param \StructType\ZwsSostContFoto $xFoto_07
     * @param \StructType\ZwsSostContFoto $xFoto_08
     * @param \StructType\ZwsSostContFoto $xFoto_09
     * @param \StructType\ZwsSostContFoto $xFoto_10
     * @param string $xImpianto
     * @param \StructType\ZwsSostContIntervento $xIntervento
     * @param \StructType\ZwsSostContCont $xNuovoCont
     * @param string $xOrdine
     * @param string $xPosizione
     * @param \StructType\ZwsSostContCont $xPreceCont
     * @param string $xTargaMezzo
     */
    public function __construct(\StructType\TableOfZwsSostContCid $tCid = null, \StructType\TableOfTline $tNotePosizione = null, \StructType\TableOfTline $tNoteSost = null, \StructType\TableOfBapireturn1 $tReturn = null, $xAccessibilita = null, $xAttivitaMezzo = null, $xAvviso = null, $xConsuntivare = null, $xCoordinate = null, $xDittaEsterna = null, $xEsito = null, \StructType\ZwsSostContFoto $xFoto_01 = null, \StructType\ZwsSostContFoto $xFoto_02 = null, \StructType\ZwsSostContFoto $xFoto_03 = null, \StructType\ZwsSostContFoto $xFoto_04 = null, \StructType\ZwsSostContFoto $xFoto_05 = null, \StructType\ZwsSostContFoto $xFoto_06 = null, \StructType\ZwsSostContFoto $xFoto_07 = null, \StructType\ZwsSostContFoto $xFoto_08 = null, \StructType\ZwsSostContFoto $xFoto_09 = null, \StructType\ZwsSostContFoto $xFoto_10 = null, $xImpianto = null, \StructType\ZwsSostContIntervento $xIntervento = null, \StructType\ZwsSostContCont $xNuovoCont = null, $xOrdine = null, $xPosizione = null, \StructType\ZwsSostContCont $xPreceCont = null, $xTargaMezzo = null)
    {
        $this
            ->setTCid($tCid)
            ->setTNotePosizione($tNotePosizione)
            ->setTNoteSost($tNoteSost)
            ->setTReturn($tReturn)
            ->setXAccessibilita($xAccessibilita)
            ->setXAttivitaMezzo($xAttivitaMezzo)
            ->setXAvviso($xAvviso)
            ->setXConsuntivare($xConsuntivare)
            ->setXCoordinate($xCoordinate)
            ->setXDittaEsterna($xDittaEsterna)
            ->setXEsito($xEsito)
            ->setXFoto_01($xFoto_01)
            ->setXFoto_02($xFoto_02)
            ->setXFoto_03($xFoto_03)
            ->setXFoto_04($xFoto_04)
            ->setXFoto_05($xFoto_05)
            ->setXFoto_06($xFoto_06)
            ->setXFoto_07($xFoto_07)
            ->setXFoto_08($xFoto_08)
            ->setXFoto_09($xFoto_09)
            ->setXFoto_10($xFoto_10)
            ->setXImpianto($xImpianto)
            ->setXIntervento($xIntervento)
            ->setXNuovoCont($xNuovoCont)
            ->setXOrdine($xOrdine)
            ->setXPosizione($xPosizione)
            ->setXPreceCont($xPreceCont)
            ->setXTargaMezzo($xTargaMezzo);
    }
    /**
     * Get TCid value
     * @return \StructType\TableOfZwsSostContCid|null
     */
    public function getTCid()
    {
        return $this->TCid;
    }
    /**
     * Set TCid value
     * @param \StructType\TableOfZwsSostContCid $tCid
     * @return \StructType\ZWsSostContatoriDatain
     */
    public function setTCid(\StructType\TableOfZwsSostContCid $tCid = null)
    {
        $this->TCid = $tCid;
        return $this;
    }
    /**
     * Get TNotePosizione value
     * @return \StructType\TableOfTline|null
     */
    public function getTNotePosizione()
    {
        return $this->TNotePosizione;
    }
    /**
     * Set TNotePosizione value
     * @param \StructType\TableOfTline $tNotePosizione
     * @return \StructType\ZWsSostContatoriDatain
     */
    public function setTNotePosizione(\StructType\TableOfTline $tNotePosizione = null)
    {
        $this->TNotePosizione = $tNotePosizione;
        return $this;
    }
    /**
     * Get TNoteSost value
     * @return \StructType\TableOfTline|null
     */
    public function getTNoteSost()
    {
        return $this->TNoteSost;
    }
    /**
     * Set TNoteSost value
     * @param \StructType\TableOfTline $tNoteSost
     * @return \StructType\ZWsSostContatoriDatain
     */
    public function setTNoteSost(\StructType\TableOfTline $tNoteSost = null)
    {
        $this->TNoteSost = $tNoteSost;
        return $this;
    }
    /**
     * Get TReturn value
     * @return \StructType\TableOfBapireturn1|null
     */
    public function getTReturn()
    {
        return $this->TReturn;
    }
    /**
     * Set TReturn value
     * @param \StructType\TableOfBapireturn1 $tReturn
     * @return \StructType\ZWsSostContatoriDatain
     */
    public function setTReturn(\StructType\TableOfBapireturn1 $tReturn = null)
    {
        $this->TReturn = $tReturn;
        return $this;
    }
    /**
     * Get XAccessibilita value
     * @return string|null
     */
    public function getXAccessibilita()
    {
        return $this->XAccessibilita;
    }
    /**
     * Set XAccessibilita value
     * @param string $xAccessibilita
     * @return \StructType\ZWsSostContatoriDatain
     */
    public function setXAccessibilita($xAccessibilita = null)
    {
        // validation for constraint: string
        if (!is_null($xAccessibilita) && !is_string($xAccessibilita)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($xAccessibilita, true), gettype($xAccessibilita)), __LINE__);
        }
        // validation for constraint: maxLength(1)
        if (!is_null($xAccessibilita) && mb_strlen($xAccessibilita) > 1) {
            throw new \InvalidArgumentException(sprintf('Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 1', mb_strlen($xAccessibilita)), __LINE__);
        }
        $this->XAccessibilita = $xAccessibilita;
        return $this;
    }
    /**
     * Get XAttivitaMezzo value
     * @return string|null
     */
    public function getXAttivitaMezzo()
    {
        return $this->XAttivitaMezzo;
    }
    /**
     * Set XAttivitaMezzo value
     * @param string $xAttivitaMezzo
     * @return \StructType\ZWsSostContatoriDatain
     */
    public function setXAttivitaMezzo($xAttivitaMezzo = null)
    {
        // validation for constraint: string
        if (!is_null($xAttivitaMezzo) && !is_string($xAttivitaMezzo)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($xAttivitaMezzo, true), gettype($xAttivitaMezzo)), __LINE__);
        }
        // validation for constraint: maxLength(6)
        if (!is_null($xAttivitaMezzo) && mb_strlen($xAttivitaMezzo) > 6) {
            throw new \InvalidArgumentException(sprintf('Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 6', mb_strlen($xAttivitaMezzo)), __LINE__);
        }
        $this->XAttivitaMezzo = $xAttivitaMezzo;
        return $this;
    }
    /**
     * Get XAvviso value
     * @return string|null
     */
    public function getXAvviso()
    {
        return $this->XAvviso;
    }
    /**
     * Set XAvviso value
     * @param string $xAvviso
     * @return \StructType\ZWsSostContatoriDatain
     */
    public function setXAvviso($xAvviso = null)
    {
        // validation for constraint: string
        if (!is_null($xAvviso) && !is_string($xAvviso)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($xAvviso, true), gettype($xAvviso)), __LINE__);
        }
        // validation for constraint: maxLength(12)
        if (!is_null($xAvviso) && mb_strlen($xAvviso) > 12) {
            throw new \InvalidArgumentException(sprintf('Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 12', mb_strlen($xAvviso)), __LINE__);
        }
        $this->XAvviso = $xAvviso;
        return $this;
    }
    /**
     * Get XConsuntivare value
     * @return string|null
     */
    public function getXConsuntivare()
    {
        return $this->XConsuntivare;
    }
    /**
     * Set XConsuntivare value
     * @param string $xConsuntivare
     * @return \StructType\ZWsSostContatoriDatain
     */
    public function setXConsuntivare($xConsuntivare = null)
    {
        // validation for constraint: string
        if (!is_null($xConsuntivare) && !is_string($xConsuntivare)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($xConsuntivare, true), gettype($xConsuntivare)), __LINE__);
        }
        // validation for constraint: maxLength(1)
        if (!is_null($xConsuntivare) && mb_strlen($xConsuntivare) > 1) {
            throw new \InvalidArgumentException(sprintf('Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 1', mb_strlen($xConsuntivare)), __LINE__);
        }
        $this->XConsuntivare = $xConsuntivare;
        return $this;
    }
    /**
     * Get XCoordinate value
     * @return string|null
     */
    public function getXCoordinate()
    {
        return $this->XCoordinate;
    }
    /**
     * Set XCoordinate value
     * @param string $xCoordinate
     * @return \StructType\ZWsSostContatoriDatain
     */
    public function setXCoordinate($xCoordinate = null)
    {
        // validation for constraint: string
        if (!is_null($xCoordinate) && !is_string($xCoordinate)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($xCoordinate, true), gettype($xCoordinate)), __LINE__);
        }
        // validation for constraint: maxLength(50)
        if (!is_null($xCoordinate) && mb_strlen($xCoordinate) > 50) {
            throw new \InvalidArgumentException(sprintf('Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 50', mb_strlen($xCoordinate)), __LINE__);
        }
        $this->XCoordinate = $xCoordinate;
        return $this;
    }
    /**
     * Get XDittaEsterna value
     * @return string|null
     */
    public function getXDittaEsterna()
    {
        return $this->XDittaEsterna;
    }
    /**
     * Set XDittaEsterna value
     * @param string $xDittaEsterna
     * @return \StructType\ZWsSostContatoriDatain
     */
    public function setXDittaEsterna($xDittaEsterna = null)
    {
        // validation for constraint: string
        if (!is_null($xDittaEsterna) && !is_string($xDittaEsterna)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($xDittaEsterna, true), gettype($xDittaEsterna)), __LINE__);
        }
        // validation for constraint: maxLength(10)
        if (!is_null($xDittaEsterna) && mb_strlen($xDittaEsterna) > 10) {
            throw new \InvalidArgumentException(sprintf('Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 10', mb_strlen($xDittaEsterna)), __LINE__);
        }
        $this->XDittaEsterna = $xDittaEsterna;
        return $this;
    }
    /**
     * Get XEsito value
     * @return string|null
     */
    public function getXEsito()
    {
        return $this->XEsito;
    }
    /**
     * Set XEsito value
     * @param string $xEsito
     * @return \StructType\ZWsSostContatoriDatain
     */
    public function setXEsito($xEsito = null)
    {
        // validation for constraint: string
        if (!is_null($xEsito) && !is_string($xEsito)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($xEsito, true), gettype($xEsito)), __LINE__);
        }
        // validation for constraint: maxLength(2)
        if (!is_null($xEsito) && mb_strlen($xEsito) > 2) {
            throw new \InvalidArgumentException(sprintf('Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 2', mb_strlen($xEsito)), __LINE__);
        }
        $this->XEsito = $xEsito;
        return $this;
    }
    /**
     * Get xFoto value
     * @return xFoto
     */
    public function getXFoto_01()
    {
        return $this->XFoto_01;
    }
    /**
     * Set xFoto value
     * @param xFoto $xFoto
     * @return \StructType\ZWsSostContatoriDatain
     */
    public function setXFoto_01(\StructType\ZwsSostContFoto $xFoto_01 = null)
    {
        $this->XFoto_01 = $xFoto_01;
        return $this;
    }
    /**
     * Get xFoto value
     * @return xFoto
     */
    public function getXFoto_02()
    {
        return $this->XFoto_02;
    }
    /**
     * Set xFoto value
     * @param xFoto $xFoto
     * @return \StructType\ZWsSostContatoriDatain
     */
    public function setXFoto_02(\StructType\ZwsSostContFoto $xFoto_02 = null)
    {
        $this->XFoto_02 = $xFoto_02;
        return $this;
    }
    /**
     * Get xFoto value
     * @return xFoto
     */
    public function getXFoto_03()
    {
        return $this->XFoto_03;
    }
    /**
     * Set xFoto value
     * @param xFoto $xFoto
     * @return \StructType\ZWsSostContatoriDatain
     */
    public function setXFoto_03(\StructType\ZwsSostContFoto $xFoto_03 = null)
    {
        $this->XFoto_03 = $xFoto_03;
        return $this;
    }
    /**
     * Get xFoto value
     * @return xFoto
     */
    public function getXFoto_04()
    {
        return $this->XFoto_04;
    }
    /**
     * Set xFoto value
     * @param xFoto $xFoto
     * @return \StructType\ZWsSostContatoriDatain
     */
    public function setXFoto_04(\StructType\ZwsSostContFoto $xFoto_04 = null)
    {
        $this->XFoto_04 = $xFoto_04;
        return $this;
    }
    /**
     * Get xFoto value
     * @return xFoto
     */
    public function getXFoto_05()
    {
        return $this->XFoto_05;
    }
    /**
     * Set xFoto value
     * @param xFoto $xFoto
     * @return \StructType\ZWsSostContatoriDatain
     */
    public function setXFoto_05(\StructType\ZwsSostContFoto $xFoto_05 = null)
    {
        $this->XFoto_05 = $xFoto_05;
        return $this;
    }
    /**
     * Get xFoto value
     * @return xFoto
     */
    public function getXFoto_06()
    {
        return $this->XFoto_06;
    }
    /**
     * Set xFoto value
     * @param xFoto $xFoto
     * @return \StructType\ZWsSostContatoriDatain
     */
    public function setXFoto_06(\StructType\ZwsSostContFoto $xFoto_06 = null)
    {
        $this->XFoto_06 = $xFoto_06;
        return $this;
    }
    /**
     * Get xFoto value
     * @return xFoto
     */
    public function getXFoto_07()
    {
        return $this->XFoto_07;
    }
    /**
     * Set xFoto value
     * @param xFoto $xFoto
     * @return \StructType\ZWsSostContatoriDatain
     */
    public function setXFoto_07(\StructType\ZwsSostContFoto $xFoto_07 = null)
    {
        $this->XFoto_07 = $xFoto_07;
        return $this;
    }
    /**
     * Get xFoto value
     * @return xFoto
     */
    public function getXFoto_08()
    {
        return $this->XFoto_08;
    }
    /**
     * Set xFoto value
     * @param xFoto $xFoto
     * @return \StructType\ZWsSostContatoriDatain
     */
    public function setXFoto_08(\StructType\ZwsSostContFoto $xFoto_08 = null)
    {
        $this->XFoto_08 = $xFoto_08;
        return $this;
    }
    /**
     * Get xFoto value
     * @return xFoto
     */
    public function getXFoto_09()
    {
        return $this->XFoto_09;
    }
    /**
     * Set xFoto value
     * @param xFoto $xFoto
     * @return \StructType\ZWsSostContatoriDatain
     */
    public function setXFoto_09(\StructType\ZwsSostContFoto $xFoto_09 = null)
    {
        $this->XFoto_09 = $xFoto_09;
        return $this;
    }
    /**
     * Get xFoto value
     * @return xFoto
     */
    public function getXFoto_10()
    {
        return $this->XFoto_10;
    }
    /**
     * Set xFoto value
     * @param xFoto $xFoto
     * @return \StructType\ZWsSostContatoriDatain
     */
    public function setXFoto_10(\StructType\ZwsSostContFoto $xFoto_10 = null)
    {
        $this->XFoto_10 = $xFoto_10;
        return $this;
    }
    /**
     * Get XImpianto value
     * @return string|null
     */
    public function getXImpianto()
    {
        return $this->XImpianto;
    }
    /**
     * Set XImpianto value
     * @param string $xImpianto
     * @return \StructType\ZWsSostContatoriDatain
     */
    public function setXImpianto($xImpianto = null)
    {
        // validation for constraint: string
        if (!is_null($xImpianto) && !is_string($xImpianto)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($xImpianto, true), gettype($xImpianto)), __LINE__);
        }
        // validation for constraint: maxLength(10)
        if (!is_null($xImpianto) && mb_strlen($xImpianto) > 10) {
            throw new \InvalidArgumentException(sprintf('Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 10', mb_strlen($xImpianto)), __LINE__);
        }
        $this->XImpianto = $xImpianto;
        return $this;
    }
    /**
     * Get XIntervento value
     * @return \StructType\ZwsSostContIntervento|null
     */
    public function getXIntervento()
    {
        return $this->XIntervento;
    }
    /**
     * Set XIntervento value
     * @param \StructType\ZwsSostContIntervento $xIntervento
     * @return \StructType\ZWsSostContatoriDatain
     */
    public function setXIntervento(\StructType\ZwsSostContIntervento $xIntervento = null)
    {
        $this->XIntervento = $xIntervento;
        return $this;
    }
    /**
     * Get XNuovoCont value
     * @return \StructType\ZwsSostContCont|null
     */
    public function getXNuovoCont()
    {
        return $this->XNuovoCont;
    }
    /**
     * Set XNuovoCont value
     * @param \StructType\ZwsSostContCont $xNuovoCont
     * @return \StructType\ZWsSostContatoriDatain
     */
    public function setXNuovoCont(\StructType\ZwsSostContCont $xNuovoCont = null)
    {
        $this->XNuovoCont = $xNuovoCont;
        return $this;
    }
    /**
     * Get XOrdine value
     * @return string|null
     */
    public function getXOrdine()
    {
        return $this->XOrdine;
    }
    /**
     * Set XOrdine value
     * @param string $xOrdine
     * @return \StructType\ZWsSostContatoriDatain
     */
    public function setXOrdine($xOrdine = null)
    {
        // validation for constraint: string
        if (!is_null($xOrdine) && !is_string($xOrdine)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($xOrdine, true), gettype($xOrdine)), __LINE__);
        }
        // validation for constraint: maxLength(12)
        if (!is_null($xOrdine) && mb_strlen($xOrdine) > 12) {
            throw new \InvalidArgumentException(sprintf('Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 12', mb_strlen($xOrdine)), __LINE__);
        }
        $this->XOrdine = $xOrdine;
        return $this;
    }
    /**
     * Get XPosizione value
     * @return string|null
     */
    public function getXPosizione()
    {
        return $this->XPosizione;
    }
    /**
     * Set XPosizione value
     * @param string $xPosizione
     * @return \StructType\ZWsSostContatoriDatain
     */
    public function setXPosizione($xPosizione = null)
    {
        // validation for constraint: string
        if (!is_null($xPosizione) && !is_string($xPosizione)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($xPosizione, true), gettype($xPosizione)), __LINE__);
        }
        // validation for constraint: maxLength(8)
        if (!is_null($xPosizione) && mb_strlen($xPosizione) > 8) {
            throw new \InvalidArgumentException(sprintf('Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 8', mb_strlen($xPosizione)), __LINE__);
        }
        $this->XPosizione = $xPosizione;
        return $this;
    }
    /**
     * Get XPreceCont value
     * @return \StructType\ZwsSostContCont|null
     */
    public function getXPreceCont()
    {
        return $this->XPreceCont;
    }
    /**
     * Set XPreceCont value
     * @param \StructType\ZwsSostContCont $xPreceCont
     * @return \StructType\ZWsSostContatoriDatain
     */
    public function setXPreceCont(\StructType\ZwsSostContCont $xPreceCont = null)
    {
        $this->XPreceCont = $xPreceCont;
        return $this;
    }
    /**
     * Get XTargaMezzo value
     * @return string|null
     */
    public function getXTargaMezzo()
    {
        return $this->XTargaMezzo;
    }
    /**
     * Set XTargaMezzo value
     * @param string $xTargaMezzo
     * @return \StructType\ZWsSostContatoriDatain
     */
    public function setXTargaMezzo($xTargaMezzo = null)
    {
        // validation for constraint: string
        if (!is_null($xTargaMezzo) && !is_string($xTargaMezzo)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($xTargaMezzo, true), gettype($xTargaMezzo)), __LINE__);
        }
        // validation for constraint: maxLength(15)
        if (!is_null($xTargaMezzo) && mb_strlen($xTargaMezzo) > 15) {
            throw new \InvalidArgumentException(sprintf('Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 15', mb_strlen($xTargaMezzo)), __LINE__);
        }
        $this->XTargaMezzo = $xTargaMezzo;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \StructType\ZWsSostContatoriDatain
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}

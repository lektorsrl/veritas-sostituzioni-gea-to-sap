StructType\ZWsSostContatoriDatainResponse Object
(
    [TCid] => StructType\TableOfZwsSostContCid Object
        (
            [item] => 
        )

    [TNotePosizione] => StructType\TableOfTline Object
        (
            [item] => 
        )

    [TNoteSost] => StructType\TableOfTline Object
        (
            [item] => 
        )

    [TReturn] => StructType\TableOfBapireturn1 Object
        (
            [item] => Array
                (
                    [0] => StructType\Bapireturn1 Object
                        (
                            [Type] => S
                            [Id] => 
                            [Number] => 000
                            [Message] => Dato registrato correttamente in SAP
                            [LogNo] => 
                            [LogMsgNo] => 000000
                            [MessageV1] => 
                            [MessageV2] => 
                            [MessageV3] => 
                            [MessageV4] => 
                        )

                    [1] => StructType\Bapireturn1 Object
                        (
                            [Type] => E
                            [Id] => ZWFM
                            [Number] => 025
                            [Message] => Presenti 0 apparecchiature disponibili con matricola A134158 e produttore 39 - WATERTECH
                            [LogNo] => 
                            [LogMsgNo] => 000000
                            [MessageV1] => 0
                            [MessageV2] => A134158
                            [MessageV3] => 39 - WATERTECH
                            [MessageV4] => 
                        )

                    [2] => StructType\Bapireturn1 Object
                        (
                            [Type] => I
                            [Id] => ZWFM
                            [Number] => 085
                            [Message] => Codifica 201/2BCM associata a processo SOST "Sostituzione massiva"
                            [LogNo] => 
                            [LogMsgNo] => 000000
                            [MessageV1] => 201
                            [MessageV2] => 2BCM
                            [MessageV3] => SOST
                            [MessageV4] => Sostituzione massiva
                        )

                    [3] => StructType\Bapireturn1 Object
                        (
                            [Type] => E
                            [Id] => ZWFM
                            [Number] => 051
                            [Message] => Nessuna apparecchiatura scelta per l'operazione
                            [LogNo] => 
                            [LogMsgNo] => 000000
                            [MessageV1] => 
                            [MessageV2] => 
                            [MessageV3] => 
                            [MessageV4] => 
                        )

                )

        )

    [YAvviso] => 004000709276
    [YClav] => CA028
    [YImpianto] => 4100324307
)
Creato record do logRecord passato allo stato SPE